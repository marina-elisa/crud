import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { FirebaseServiceProvider } from '../providers/firebase-service/firebase-service';
import { HelloPage } from '../pages/hello/hello';
import { HelloPageModule } from '../pages/hello/hello.module';
import { FeedpagePageModule } from '../pages/feedpage/feedpage.module';
import { FeedpagePage } from '../pages/feedpage/feedpage';
import { ProfileProvider } from '../providers/profile/profile';

export const firebaseConfig = {
    apiKey: "AIzaSyBNDYGHyXo9ocsy-yDznBcS-m9DovuLU-E",
    authDomain: "crud2-53032.firebaseapp.com",
    databaseURL: "https://crud2-53032.firebaseio.com",
    projectId: "crud2-53032",
    storageBucket: "crud2-53032.appspot.com",
    messagingSenderId: "395032527946"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    HelloPageModule,
    FeedpagePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HelloPage,
    FeedpagePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseServiceProvider,
    ProfileProvider
  ]
})
export class AppModule {}
