import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class FirebaseServiceProvider {
  
  userId: string;

  constructor(public db: AngularFireDatabase, public auth: AngularFireAuth) {
    this.auth.authState.subscribe(user => {
      if(user)
       this.userId = user.uid
    })
  }

  save(userdata: any) {
    this.db.list('users')
    .push(userdata)
    .then(r => console.log(r));
    }
    
}
