import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, NavController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseServiceProvider } from '../../providers/firebase-service/firebase-service';
import * as firebase from 'Firebase';
import { HelloPage } from '../hello/hello';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public user: any;
  public userId: string;
  @ViewChild('nome') name;
  @ViewChild('usuario') email;
  @ViewChild('senha') password;
  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public firebaseauth: AngularFireAuth,
    public database: AngularFireDatabase,
    public navParams: NavParams) {
    firebaseauth.user.subscribe((data => {
      this.user = data;
    }));
    firebaseauth.authState.subscribe(user => {
      if (user)
      this.userId = user.uid
    })
  }
  public LoginComEmail(): void {
    this.firebaseauth.auth.signInWithEmailAndPassword(this.email.value, this.password.value)
      .then(() => {
        this.exibirToast('Login efetuado com sucesso');
        this.navCtrl.setRoot(HelloPage);
      })
      .catch((erro: any) => {
        this.exibirToast(erro);
      });
  }
  public cadastrarUsuario(): void {
    this.firebaseauth.auth.createUserWithEmailAndPassword(this.email.value, this.password.value)
      .then(() => {
        this.exibirToast('Usuário criado com sucesso');
      })
      .catch((erro: any) => {
        this.exibirToast(erro);
      });
  }
  public Sair(): void {
    this.firebaseauth.auth.signOut()
      .then(() => {
        this.exibirToast('Você saiu');
      })
      .catch((erro: any) => {
        this.exibirToast(erro);
      });
  }
  public criarUsuario(): void {
    this.database.list('users/')
    .update(this.userId, { name: this.name, type: 'regular'}) 
    this.navCtrl.setRoot(HelloPage)
    
  }
  private exibirToast(mensagem: string): void {
    let toast = this.toastCtrl.create({
      duration: 3000,
      position: 'botton'
    });
    toast.setMessage(mensagem);
    toast.present();
  }
  public gotoHelloPage() {
    this.navCtrl.push(HelloPage);
  }
}