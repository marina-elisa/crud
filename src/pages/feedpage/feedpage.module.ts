import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedpagePage } from './feedpage';

@NgModule({
  declarations: [
    FeedpagePage,
  ],
  imports: [
    IonicPageModule.forChild(FeedpagePage),
  ],
})
export class FeedpagePageModule {}
